var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Communes = require('../models/communes');

var communeRouter = express.Router();
communeRouter.use(bodyParser.json());

communeRouter.route('/')
    .get(function (req, res, next) {
        Communes.find({}, function (err, zone) {
            if (err) throw err;
            res.json(zone);
        });
    });

// .post(function (req, res, next) {
//     Communes.create(req.body, function (err, dish) {
//         if (err) throw err;
//         console.log('Dish created!');
//         var id = dish._id;
//
//         res.writeHead(200, {
//             'Content-Type': 'text/plain'
//         });
//         res.end('Added the dish with id: ' + id);
//     });
// })
//
// .delete(function (req, res, next) {
//     Communes.remove({}, function (err, resp) {
//         if (err) throw err;
//         res.json(resp);
//     });
// });


// .put(function (req, res, next) {
//     Communes.findByIdAndUpdate(req.params.dishId, {
//         $set: req.body
//     }, {
//         new: true
//     }, function (err, dish) {
//         if (err) throw err;
//         res.json(dish);
//     });
// })
//
// .delete(function (req, res, next) {
//     Communes.findByIdAndRemove(req.params.dishId, function (err, resp) {        if (err) throw err;
//         res.json(resp);
//     });
// });

module.exports = communeRouter;