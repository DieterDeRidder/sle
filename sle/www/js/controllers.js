angular.module('sle.controllers', [])

  //Controller for the side menue
  .controller('AppCtrl', function ($scope, toggleManualStateFactory) {
    $scope.toggleValue = false;

    //toggle the toggleValue on $scope and set it on the toggleManualStateFactory factory so that it's available in the HomeController
    $scope.toggleManualMode = function() {
      $scope.toggleValue = !$scope.toggleValue;
      console.log('$scope.toggleValue=' + $scope.toggleValue);
      toggleManualStateFactory.manualState = $scope.toggleValue;
    };

  })


  .controller('HomeController', ['$scope', '$state', 'zoneFactory', 'toggleManualStateFactory', 'baseURL', function ($scope, $state, zoneFactory, toggleManualStateFactory, baseURL) {
    $scope.$on('$ionicView.enter', function(e) {
      console.log('toggleManualStateFactory.manualState=' + toggleManualStateFactory.manualState);

      if (toggleManualStateFactory.manualState === false) { //not in manual state -> use geolocation lat/lon to determine zone
        var posOptions = {timeout: 10000, enableHighAccuracy: true};
        navigator.geolocation.getCurrentPosition(  //use device's geolocation service to retrieve latitude/longitude in WGS format
          function (position) {
            $scope.lat = position.coords.latitude;
            $scope.long = position.coords.longitude;
            console.log("lat=" + $scope.lat + "long=" + $scope.long);
            $scope.zone = zoneFactory.byLatLon({lat: $scope.lat, lon: $scope.long}); //pass the latitude/longitude to the zoneFactory to do a GET /zones/:lat/:lon on backend
          },
          function (err) {
            console.log("geolocation error" + err);
            $scope.zone = null;
          },
          posOptions);
      } else { // manual state -> use the zone that was set in the ManualController
        $scope.zone = toggleManualStateFactory.selectedManualZone;
      }
    });

    $scope.baseURL = baseURL;
    $scope.carDetail = function (car) { //the car param was obtained by clicking a car in the list.  Pass it to the app.car details state.
      $state.go('app.car', {carObject: car, zoneName: $scope.zone.name});
    };
    $scope.trajectDetail = function (traject) { //same for trajects
      $state.go('app.traject', {trajectObject: traject, zoneName: $scope.zone.name});
    };
    $scope.deviceDetail = function (device) { //same for devices
      $state.go('app.device', {deviceObject: device, zoneName: $scope.zone.name});
    };


  }])
.controller('CarController', ['$scope', '$stateParams', 'baseURL', function ($scope, $stateParams, baseURL) {
    console.log('baseURL=' + baseURL);
    $scope.baseURL = baseURL;
    $scope.car = $stateParams.carObject;
    $scope.zoneName = $stateParams.zoneName;
    console.log('car:' + JSON.stringify($stateParams.carObject));
    console.log('zoneName:' + $stateParams.zoneName);
  }])
  .controller('TrajectController', ['$scope', '$stateParams', 'baseURL', function ($scope, $stateParams, baseURL) {
    console.log('baseURL=' + baseURL);
    $scope.baseURL = baseURL;
    $scope.traject = $stateParams.trajectObject;
    $scope.zoneName = $stateParams.zoneName;
    console.log('traject:' + JSON.stringify($stateParams.trajectObject));
    console.log('zoneName:' + $stateParams.zoneName);
  }])
  .controller('DeviceController', ['$scope', '$stateParams', 'baseURL', function ($scope, $stateParams, baseURL) {
    console.log('baseURL=' + baseURL);
    $scope.baseURL = baseURL;
    $scope.device = $stateParams.deviceObject;
    $scope.zoneName = $stateParams.zoneName;
    console.log('device:' + JSON.stringify($stateParams.deviceObject));
    console.log('zoneName:' + $stateParams.zoneName);
  }])
  .controller('ManualController', ['$scope', 'communities', 'zoneFactory', 'toggleManualStateFactory', 'baseURL', function ($scope, communities, zoneFactory, toggleManualStateFactory, baseURL) {
    $scope.baseURL = baseURL;
    $scope.communities = communities;

    $scope.setManualZone = function(selected) {
      if (selected.zone !== null) {
        console.log('in selected.zone != null');
        zoneFactory.get({id: selected.zone}, function (zone) {
          toggleManualStateFactory.selectedManualZone = zone; //A commune from the communes list was selected. Use it's zone (id) field to retrieve the zone belonging to the commune.
        });
      } else {
        toggleManualStateFactory.selectedManualZone = null;
      }
    };
  }]);
