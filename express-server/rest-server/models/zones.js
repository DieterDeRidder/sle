
// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var carSchema = new Schema({
    model: {
        type: String,
        required: true
    },
    license: {
        type: String,
        required: false
    },
    color: {
        type: String,
        required: false
    },
    picture: {
        type: String,
        required: false
    },
    thumbnail: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

var trajectSchema = new Schema({
    speed: {
        type: Number,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    picture: {
        type: String,
        required: false
    },
    thumbnail: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

var deviceSchema = new Schema({
    type: {
        type: String,
        required: true
    },
    technology: {
        type: String,
        required: false
    },
    picture: {
        type: String,
        required: false
    },
    thumbnail: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

// create a schema
var zoneSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    items: {
        cars: [carSchema],
        traject: [trajectSchema],
        device: [deviceSchema]
    },
    exchange: {
        type: [Schema.Types.ObjectId]
    }
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Zones = mongoose.model('Zone', zoneSchema);

// make this available to our Node applications
module.exports = Zones;