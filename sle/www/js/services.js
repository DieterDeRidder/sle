'use strict';

angular.module('sle.services', ['ngResource'])
  .constant("baseURL", "http://vps362773.ovh.net:3000/")
  .factory('zoneFactory', ['$resource', 'baseURL', function($resource, baseURL) {
      return $resource(baseURL + "zones/:id", {},
        {byLatLon: {
          url: baseURL + 'zones/:lat/:lon',
          method: 'GET'
        }}
      );
    }])
  .factory('communitiesFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    console.log('in comFactory: ' + baseURL + "communes");
    return $resource(baseURL + "communes");
  }])
  .factory('toggleManualStateFactory', function() { //Factory that shares properties between controllers
    return {'manualState': false, 'selectedManualZone': null};
});
