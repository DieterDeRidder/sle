var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var request = require('request');

var Zones = require('../models/zones');
var Communes = require('../models/communes');

var zoneRouter = express.Router();
zoneRouter.use(bodyParser.json());

zoneRouter.route('/')
    .get(function (req, res, next) {
        Zones.find({}, function (err, zone) {
            if (err) throw err;
            res.json(zone);
        });
    });

    // .post(function (req, res, next) {
    //     Zones.create(req.body, function (err, dish) {
    //         if (err) throw err;
    //         console.log('Dish created!');
    //         var id = dish._id;
    //
    //         res.writeHead(200, {
    //             'Content-Type': 'text/plain'
    //         });
    //         res.end('Added the dish with id: ' + id);
    //     });
    // })
    //

zoneRouter.route('/:zoneId')
    .get(function (req, res, next) {
        Zones.findOne({'id': req.params.zoneId}, function (err, zone) {
            if (err) res.status(500).send('error on Zones.findOne with id:' + req.params.zoneId);
            if (!zone ||zone === null) {
                res.status(404).send('zone not found in zones collection');
                return;
            }
            res.json(zone);
        });
    });

zoneRouter.route('/:latitude/:longitude')
    .get(function (req, res, next) {
        var geoLocationUrl = "http://loc.geopunt.be/v3/Location?latlon=" + req.params.latitude + "," +  req.params.longitude;
        console.log('geoLocationUrl=' + geoLocationUrl);

        //Call the governmental geolocation service with latitude and longitude params
        request.get(geoLocationUrl, function(error, response, body) {

            if (!error && response.statusCode == 200) { //response from geoloc service was ok
                var geoLocationResult = JSON.parse(response.body);

                //The json result array was not empty
                if (typeof geoLocationResult['LocationResult'] !== 'undefined' && geoLocationResult['LocationResult'].length > 0) {
                    console.log('geoLocationResult=' + geoLocationResult['LocationResult'][0]['FormattedAddress']);

                    //The Formatted Address property contains a full address with street, house, zip and commune.
                    //We only need zip and commune => parse them out of the FormattedAddress.
                    //(The json result from the geolocation service does not contain separate zip and commune properties.)
                    var regex = /,([^,]+)$/; //Filters [, 2470 Retie] out of [Brug 1 1, 2470 Retie]
                    var result = regex.exec(geoLocationResult['LocationResult'][0]['FormattedAddress']);
                    var address = result[0].substring(2); // Get rid of the [, ] in [, 2470 Retie]
                    console.log('address=' + address);

                    var zipRegex = /\d{4}/;  //Filter [2470] out of [2470 Retie]
                    var communeRegex = /[^0-9\s].*$/; //Filter [Retie] out of [2470 Retie]
                    var zip = zipRegex.exec(address)[0];
                    var commune = communeRegex.exec(address)[0];
                    //We parsed the zip and commune out of the FormattedAddress
                    console.log('zip=' + zip);
                    console.log('commune=' + commune);

                    //Do a findOne on the communes collection with zipcode and commune case insensitive.  Fetch the zone id
                    //for that commune.  Use the zone id to do a findOne in the Zones collection and return the zone.
                    Communes.findOne({"zip":zip, "place": {$regex : new RegExp(commune, "i")}}, function (err, commune) {
                        if (err) res.status(500).send({error: 'error on Communes.findOne'});
                        if (commune === null) {
                            res.status(404).send({error: 'Communes.findOne did not return a result'});
                            console.log('Communes.findOne did not return a result');
                            return;
                        }
                        Zones.findOne({'id': commune.zone}, function (err, zone) {
                            if (err) res.status(500).send({error: 'error on Zones.findOne with zone id=' + commune.zone});
                            console.log('zone=' +  JSON.stringify(zone));
                            res.json(zone);
                        });

                    });
                } else {
                    //The json result was empty => respond with a 404 NOT FOUND, meaning no address found and hence no police zone found.
                    console.log('geoLocationResult empty:' + JSON.stringify(geoLocationResult) + '.  Sending 404.');
                    res.status(404).send();
                }

            } else {
                //geolocation service gave an error or an unexpected response status.  Send a 400 BAD REQUEST
                if (response) console.log('response status:' + response.status);
                console.log('error=' + error);
                res.status(400).send();
            }
        });
    });


    // .put(function (req, res, next) {
    //     Zones.findByIdAndUpdate(req.params.dishId, {
    //         $set: req.body
    //     }, {
    //         new: true
    //     }, function (err, dish) {
    //         if (err) throw err;
    //         res.json(dish);
    //     });
    // })
    //
    // .delete(function (req, res, next) {
    //     Zones.findByIdAndRemove(req.params.dishId, function (err, resp) {        if (err) throw err;
    //         res.json(resp);
    //     });
    // });

module.exports = zoneRouter;