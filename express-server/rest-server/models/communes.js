
// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var communeSchema = new Schema({
    zip: {
        type: String,
        required: true
    },
    place: {
        type: String,
        required: true
    },
    fusion: {
        type: String,
        required: true
    },
    province: {
        type: String,
        required: true
    },
    zone: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Communes = mongoose.model('Commune', communeSchema);

// make this available to our Node applications
module.exports = Communes;