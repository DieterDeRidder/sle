// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('sle', ['ionic', 'sle.controllers', 'sle.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidebar.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeController'
      }
    }
  })

  .state('app.manual', {
      url: '/manual',
      views: {
        'menuContent': {
          templateUrl: 'templates/manual.html',
          controller: 'ManualController',
          resolve: {
            communities: ['communitiesFactory', function(communitiesFactory) {
              console.log('in state manual');
              return communitiesFactory.query();
            }]
          }
        }
      }
    })
    .state('app.addNew', {
      url: '/addNew',
      views: {
        'menuContent': {
          templateUrl: 'templates/addNew.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.car', {
    url: '/car',
    views: {
      'menuContent': {
        templateUrl: 'templates/car.html',
        controller: 'CarController'
      }
    },
    params: {
      carObject: null,
      zoneName: null
    }
  })
  .state('app.traject', {
    url: '/traject',
    views: {
      'menuContent': {
        templateUrl: 'templates/traject.html',
        controller: 'TrajectController'
      }
    },
    params: {
      trajectObject: null,
      zoneName: null
    }
  })
  .state('app.device', {
    url: '/device',
    views: {
      'menuContent': {
        templateUrl: 'templates/device.html',
        controller: 'DeviceController'
      }
    },
    params: {
      deviceObject: null,
      zoneName: null
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
